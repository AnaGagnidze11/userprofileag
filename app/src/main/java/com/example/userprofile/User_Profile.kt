package com.example.userprofile

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_user__profile.*

class User_Profile : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user__profile)
        init()
    }

    private fun init() {
        Glide.with(this)
            .load("https://i.pinimg.com/474x/21/7d/8c/217d8cac29b97aa9c59935c27e238e32.jpg")
            .into(coverImageView)

        Glide.with(this)
            .load("https://i.pinimg.com/474x/8a/16/3d/8a163d921dfd098bc68fd58e18317d24.jpg")
            .into(profileImageView)

        extractInfo()
    }

    private fun extractInfo() {
        val name = intent.extras?.getString("name", "")
        val lastName = intent.extras?.getString("last_name", "")
        val age = intent.extras?.getInt("age", 0)
        val color = intent.extras?.getString("color", "")
        val number = intent.extras?.getInt("number", 0)

        nameTextView.text = name
        lastNameTextView.text = lastName
        ageTextView.text = age.toString()
        colorTextView.text = color
        numberTextView.text = number.toString()

    }


}